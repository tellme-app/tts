import os
import torch
import time
import IPython
from IPython.display import Audio
from pathlib import Path
import wave
import requests
import json

from TTS.utils.generic_utils import setup_model
from TTS.utils.io import load_config
from TTS.utils.text.symbols import symbols, phonemes
from TTS.utils.audio import AudioProcessor
from TTS.utils.synthesis import synthesis

import time
import torch
from espnet_model_zoo.downloader import ModelDownloader
from espnet2.bin.tts_inference import Text2Speech
from parallel_wavegan.utils import download_pretrained_model
from parallel_wavegan.utils import load_model

# web
from aiohttp import web
import aiohttp_cors

here = Path(__file__).resolve().parent

routes = web.RouteTableDef()


tag = "kan-bayashi/ljspeech_conformer_fastspeech2"
vocoder_tag = "ljspeech_parallel_wavegan.v3"
fs, lang = 22050, "English"

d = ModelDownloader()
text2speech = Text2Speech(
    **d.download_and_unpack(tag),
    device="cuda",
    # Only for Tacotron 2
    threshold=0.5,
    minlenratio=0.0,
    maxlenratio=10.0,
    use_att_constraint=False,
    backward_window=1,
    forward_window=3,
    # Only for FastSpeech & FastSpeech2
    speed_control_alpha=1.0,
)
text2speech.spc2wav = None  # Disable griffin-lim
# NOTE: Sometimes download is failed due to "Permission denied". That is 
#   the limitation of google drive. Please retry after serveral hours.
vocoder = load_model(download_pretrained_model(vocoder_tag)).to("cuda").eval()
vocoder.remove_weight_norm()

use_cuda = True

TTS_MODEL = "tts_model.pth.tar"
TTS_CONFIG = "config.json"
VOCODER_MODEL = "vocoder_model.pth.tar"
VOCODER_CONFIG = "config_vocoder.json"

TTS_CONFIG = load_config(TTS_CONFIG)
VOCODER_CONFIG = load_config(VOCODER_CONFIG)

ap = AudioProcessor(**TTS_CONFIG.audio)

speaker_id = None
speakers = []

# load the model
num_chars = len(phonemes) if TTS_CONFIG.use_phonemes else len(symbols)
model = setup_model(num_chars, len(speakers), TTS_CONFIG)

# load model state
cp =  torch.load(TTS_MODEL, map_location=torch.device('cpu'))

# load the model
model.load_state_dict(cp['model'])
if use_cuda:
    model.cuda()
model.eval()

# set model stepsize
if 'r' in cp:
    model.decoder.set_r(cp['r'])

from TTS.vocoder.utils.generic_utils import setup_generator

# LOAD VOCODER MODEL
vocoder_model = setup_generator(VOCODER_CONFIG)
vocoder_model.load_state_dict(torch.load(VOCODER_MODEL, map_location="cpu")["model"])
vocoder_model.remove_weight_norm()
vocoder_model.inference_padding = 0

ap_vocoder = AudioProcessor(**VOCODER_CONFIG['audio'])    
if use_cuda:
    vocoder_model.cuda()
vocoder_model.eval()

def ttsnew(sentence):
    x = sentence

    with torch.no_grad():
        start = time.time()
        wav, c, *_ = text2speech(x)
        wav = vocoder.inference(c)
    rtf = (time.time() - start) / (len(wav) / fs)
    print(f"RTF = {rtf:5f}")

    # let us listen to generated samples
    # from IPython.display import display, Audio
    return wav.view(-1).cpu().numpy()
    # display(Audio(wav.view(-1).cpu().numpy(), rate=fs))

def tts(model, text, CONFIG, use_cuda, ap, use_gl, figures=True):
    t_1 = time.time()
    waveform, alignment, mel_spec, mel_postnet_spec, stop_tokens, inputs = synthesis(model, text, CONFIG, use_cuda, ap, speaker_id, style_wav=None,
                                                                             truncated=False, enable_eos_bos_chars=CONFIG.enable_eos_bos_chars)
    # mel_postnet_spec = ap._denormalize(mel_postnet_spec.T)
    if not use_gl:
        waveform = vocoder_model.inference(torch.FloatTensor(mel_postnet_spec.T).unsqueeze(0))
        waveform = waveform.flatten()
    if use_cuda:
        waveform = waveform.cpu()
    waveform = waveform.numpy()
    rtf = (time.time() - t_1) / (len(waveform) / ap.sample_rate)
    tps = (time.time() - t_1) / len(waveform)
    print(waveform.shape)
    print(" > Run-time: {}".format(time.time() - t_1))
    print(" > Real-time factor: {}".format(rtf))
    print(" > Time per step: {}".format(tps))
    IPython.display.display(IPython.display.Audio(waveform, rate=CONFIG.audio['sample_rate']))  
    return alignment, mel_postnet_spec, stop_tokens, waveform



@routes.post('/synth')
async def synth(request):
    redis_set_url = 'http://40.114.54.96:8003/set_audio'
    redis_get_url = 'http://40.114.54.96:8003/get_audio'

    data = await request.json()

    sentence = data['text']
    if sentence[-1] not in ".!?":
        sentence += '.'

    response = requests.post(redis_get_url, data=json.dumps({'text':sentence}))
    try:
        response.json()
        # align, spec, stop_tokens, wav = tts(model, sentence, TTS_CONFIG, use_cuda, ap, use_gl=False, figures=True)
        wav = ttsnew(sentence)
        audio = Audio(wav, rate=22050)
        audio = audio.data
        requests.post(redis_set_url, files={"file":audio}, data={"data":json.dumps({'text':sentence})})
    except Exception as e:
        audio = response.content

    response = web.StreamResponse(headers={
        'Content-Type': 'audio/wav',
        'Accept-ranges': 'bytes'
    })
    await response.prepare(request)

    # while True:
    #     datt = audio.data.read(1024)
    #     if not datt:
    #         break
    #     await response.write(datt)
    await response.write(audio)

    return response

    # return web.json_response({"audio": audio.data})

app = web.Application()
# aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader(str(here)))
app.add_routes(routes)

cors = aiohttp_cors.setup(app, defaults={
    "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        )
})
# app.router.add_post('/', store_mp3_handler)
for route in list(app.router.routes()):
    cors.add(route)
#web.run_app(app)
web.run_app(app,host='0.0.0.0', port = '8002')

    
# sentence =  "We have a responsibility to try to convince both parties to return to negotiations that will lead to peace in the Middle East."
# t = time.time()

# print(time.time() - t)
# #print(spec)




# f = open('0.wav', 'wb')
# f.write(audio.data)
# f.close()

#with wave.open('test.wav', 'wb') as f:
#    f.setnchannels(1)
#    f.setframerate(22050)
#    f.writeframesraw(wav)
